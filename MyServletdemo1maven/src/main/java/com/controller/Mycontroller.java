package com.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Mycontroller extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		PrintWriter out = response.getWriter();
//	out.print("welcome to maven");
		String firstname = request.getParameter("firstname");
		String lastname = request.getParameter("lastname");
		String s = request.getParameter("sapid");
		int sapid = Integer.parseInt(s);
		String a = request.getParameter("age");
		int age = Integer.parseInt(a);
		String gender = request.getParameter("gender");
		String[] arr = request.getParameterValues("qualifiction");
		out.print("<!DOCTYPE html>");
		out.print("<HTML>");
		out.print("<HEAD> <TITLE>hello</TITLE> </HEAD>");
		out.print("<BODY>");
		out.print("First name is:" + firstname+"<br>");
		out.print("Last name is:" + lastname+"<br>");
		out.print("SapId is:" + sapid+"<br>");
		out.print("Age name is:" + age+"<br>");
		out.print("Gender is:" + gender+"<br>");
		out.print( "List of qualifications: ");
		for (int i = 0; i < arr.length; i++) {
			out.print(arr[i] + ", ");

		}
		out.print("</BODY>");
		out.print("</HTML>");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}
