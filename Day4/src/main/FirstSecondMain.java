package main;
import model.FirstClass;
import model.SecondClass;
public class FirstSecondMain {
	public static void main(String[] args)
	{
		FirstClass objA=new FirstClass();
		SecondClass objB=new SecondClass();
		System.out.println("in main():");
		System.out.println("objA="+objA.getFirstClass());
		System.out.println("objB="+objB.getSecondClass());
		objA.setFirstClass(222);
		objB.setSecondClass(333.33);
		System.out.println("objA="+objA.getFirstClass());
		System.out.println("objB="+objB.getSecondClass());
		
		
		
	}

}
