package main;

import model.Card;
import model.MembershipCard;
import model.PaybackCard;
import java.util.Scanner;

public class CardMain {
	public static void main(String[] args)
	{
		Scanner scanner=new Scanner(System.in);
		System.out.println("select the card 1.payback card  2.membership card");
		int n=scanner.nextInt();
		if(n==1)
		{
			System.out.println("enter card details");
			System.out.println("card holder name:");
			String holderName=scanner.next();
			System.out.println("card number:");
			String cardNumber=scanner.next();
			System.out.println("expiry date");
			String expiry=scanner.next();
			System.out.println("total points earned:");
			int points=scanner.nextInt();
			System.out.println("total amount:");
			double amount=scanner.nextDouble();
			
			PaybackCard paycard=new PaybackCard(holderName, cardNumber, expiry,points, amount);
			System.out.println(paycard.toString());
		}
	}

}
