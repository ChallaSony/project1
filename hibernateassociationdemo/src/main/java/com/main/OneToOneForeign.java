package com.main;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import com.config.HibernateUtil;
import com.model.Account;
import com.model.Employee;


public class OneToOneForeign {

	public static void main(String[] args){
		SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
		Session session=sessionFactory.openSession();
		Employee employee=new Employee();
		employee.setEmpId(10);
		employee.setFirstName("challa");
		employee.setLastName("sony");
		Account account=new Account();
		account.setAcctNo(1);
		account.setAccountName("savings");
		account.setEmployee(employee);
		employee.setAccount(account);
		session.beginTransaction();
		session.save(account);
		session.save(employee);
		session.getTransaction().commit();
		System.out.println("the end");
		HibernateUtil.shutdown();
		
		
	}

}
