package com.hcl.petpeers.service;

import com.hcl.petpeers.model.User;

/**
 * 
 * @author C SONY
 *
 */
public interface LoginService {

	User validateLogin(User user);
	
}
