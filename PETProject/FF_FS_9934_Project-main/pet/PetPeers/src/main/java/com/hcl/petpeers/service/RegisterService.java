package com.hcl.petpeers.service;

import com.hcl.petpeers.model.User;

/**
 * @author haneef
 *
 */
public interface RegisterService {
	String RegisterUserDetails(User user);
}
