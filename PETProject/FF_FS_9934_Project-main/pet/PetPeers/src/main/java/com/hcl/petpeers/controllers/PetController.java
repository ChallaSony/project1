package com.hcl.petpeers.controllers;
/**
 * @author rgj87333
 */
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hcl.petpeers.model.Pet;
import com.hcl.petpeers.model.User;
import com.hcl.petpeers.repository.PetDao;
import com.hcl.petpeers.service.PetService;

@Controller
public class PetController {

	@Autowired
	PetService petService;
	
	@RequestMapping("/registerpet")
	public String getPetRegisterPage(Model model,HttpServletResponse response) {
		Pet pet = new Pet();
		model.addAttribute("pet", pet);
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
		response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
		response.setDateHeader("Expires", 0);
		return "registerpet";
	}
	
	@PostMapping("/processregisterpet")
	public String register( @ModelAttribute("pet") Pet pet, 
			RedirectAttributes redirectAttributes, Model model,HttpServletRequest request) {

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("validUser");
		System.out.println(user.getEmail());
		String message = petService.addPet(pet, user);
		if (message.equals("Sucessfully Registered")) {
			
			
			System.out.println("registerd sucess");
			return "redirect:/home";
		
		} else {
			model.addAttribute("message", "not registered");
			return "registerpet";
		}

	}
	
	@RequestMapping("/displaypets")
	public String getMyPets(Model model,HttpServletRequest request,HttpServletResponse response) {
		
		HttpSession session =  request.getSession();
		User user = (User) session.getAttribute("validUser");
		List<Pet> myPets = petService.displayPets(user);
		model.addAttribute("myPets", myPets);
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
		response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
		response.setDateHeader("Expires", 0);
		return "mypets";
		
	}

	/*@RequestMapping("/home")
	public String getBuyPetsPage(Model model,HttpServletResponse response) {
		
		List<Pet> availPets = petservice.displayPetsAvailable();
		model.addAttribute("availPets", availPets);
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
		response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
		response.setDateHeader("Expires", 0);
		return "home";
	}
	
	@RequestMapping("/buypet/{petId}")
	public String getBuyPetPage(@PathVariable int petId,Model model,HttpServletRequest httpServletRequest) {
		String message ="";
		HttpSession session = httpServletRequest.getSession();
		User user = (User) session.getAttribute("validUser");
		Pet pet = petservice.buyPet(petId, user);
		if(pet==null) {
			 message ="Sorry your Purchase Failed..!!";
			model.addAttribute("message",message);
		}
		message="Your purchase is success!! ";
		model.addAttribute("updated_pet",pet);
		model.addAttribute("message",message);
		return "buypet";
		
	}*/
}


