package com.hcl.petpeers.repository;

import com.hcl.petpeers.model.User;

/**
 * @author haneef
 *
 */
public interface LoginDao {

 User verifyUser(User user);
	
}
