package com.hcl.petpeers.repository;

import com.hcl.petpeers.model.User;

/**
 * @author haneef
 *
 */
public interface UserRegisterDao {

	String getUserDetails(User user);
}
