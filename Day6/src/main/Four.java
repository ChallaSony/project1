package main;

/*Rewrite the earlier program so that, now the class DemoThread1 instead of
implementing from Runnable interface, 
will now extend from Thread class. */
public class Four extends Thread{

	public static void main(String[] args) {
		Four obj1 = new Four();
		Four obj2 = new Four();
		Four obj3 = new Four();
		obj1.run();

	}

	public Four() {

		First t1 = new First();

		System.out.println("Name of t1:" + t1.getName());

		t1.start();

	}

	@Override
	public void run() {
		try {
		System.out.println("running child Thread in loop:");
		int counter = 1;
		
		while (counter < 11) {
			System.out.println(counter);
			counter = counter + 1;
			
				Thread.sleep(2000);
			} }catch (InterruptedException e) {

				e.printStackTrace();
			}
		}
	}

