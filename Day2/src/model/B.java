package model;

public class B extends A{
	int employeeExperience;

	public B(int employeeId, String employeeName, int employeeAge, int employeeExperience) {
		super(employeeId, employeeName, employeeAge);
		this.employeeExperience = employeeExperience;
	}

	@Override
	public void display() {
		super.display();
		System.out.println("employee ID is:"+super.employeeId);
		System.out.println("employee Name is:"+super.employeeName);
		System.out.println("employee Age is:"+super.employeeAge);
		System.out.println("employee Experience is:"+employeeExperience);
	}
	

}
