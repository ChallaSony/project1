package model;

public class A {
	int employeeId;
	String employeeName;
	int employeeAge;
	public A(int employeeId, String employeeName, int employeeAge) {
		super();
		this.employeeId = employeeId;
		this.employeeName = employeeName;
		this.employeeAge = employeeAge;
	}
	public void display()
	{
		System.out.println("Parent class");
	}
}
