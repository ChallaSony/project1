package main;

public class Parent {

}
class Child1 extends Parent{}
class Child2 extends Parent{}
class Test
{
	public static void main(String[] args)
	{
		Parent p=new Parent();
		Child1 ch1=new Child1();
		Child2 ch2=new Child2();
		System.out.println(ch1 instanceof Parent);
		System.out.println(ch2 instanceof Parent);
		System.out.println(p instanceof Child1);
		System.out.println(p instanceof Child2);
		p=ch1;
		System.out.println(p instanceof Child1);
		System.out.println(p instanceof Child2);
		p=ch2;
		System.out.println(p instanceof Child1);
		System.out.println(p instanceof Child2);
		
	}
}
