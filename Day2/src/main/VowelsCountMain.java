package main;
import java.util.Scanner;

import model.VowelsCount;
public class VowelsCountMain {
	public static void main(String[] args)
	{
		Scanner scanner=new Scanner(System.in);
		System.out.println("Enter the string:");
		String str=scanner.next();
		VowelsCount vowelscount=new VowelsCount();
		System.out.println("The total vowels in a string is:"+vowelscount.vowelsCount(str));
	}

}
