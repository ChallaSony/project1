package com.dao;


import org.hibernate.Session;
import org.hibernate.Transaction;

import com.model.Employee;

public class EmployeeDAOImpl implements EmployeeDAO {

	public Employee createEmployee(Employee employee) {
		Session session=SessionFactoryUtil.getSession();
		Transaction transaction=session.beginTransaction();
		Integer integer=(Integer)session.save(employee);
		transaction.commit();
		System.out.println("employee created successfully");
		return employee;
	}

	public Employee readEmployee(int empNo) {
		Session session=SessionFactoryUtil.getSession();
		return session.get(Employee.class, empNo);
		
	}

	public int updateEmployee(Employee employee) {
		Session session=SessionFactoryUtil.getSession();
		Transaction transaction=session.beginTransaction();
		session.saveOrUpdate(employee);
		transaction.commit();
		return employee.getEmpNo();
	}

	public Employee deleteEmployee(int empNo) {
		Employee employee=readEmployee(empNo);
		Session session=SessionFactoryUtil.getSession();
		Transaction transaction=session.beginTransaction();
		session.delete(employee);
		transaction.commit();
		return employee;
	}

}
