package com.dao;

import com.model.Employee;

public interface EmployeeDAO {
	public abstract Employee createEmployee(Employee employee);

	public abstract Employee readEmployee(int empNo);

	public abstract int updateEmployee(Employee employee);

	public abstract Employee deleteEmployee(int empNo);

}
