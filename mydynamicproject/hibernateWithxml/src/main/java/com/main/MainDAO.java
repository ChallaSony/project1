package com.main;

import java.util.Scanner;

import com.dao.EmployeeDAO;
import com.dao.EmployeeDAOImpl;
import com.model.Employee;

public class MainDAO {

	public static void main(String[] args) {
		EmployeeDAO dao = new EmployeeDAOImpl();
		/*
		 * Employee employee=new Employee(22,"assigned"); Employee
		 * ret=dao.createEmployee(employee);
		 * System.out.println("employee inserted :"+ret.getEmpName());
		 */
		/*
		 * Scanner scanner = new Scanner(System.in);
		 * System.out.println("enter employee number:"); int id = scanner.nextInt();
		 * Employee obj = dao.readEmployee(id);
		 * System.out.println("emp no in database:"+obj.getEmpNo());
		 * System.out.println("emp name in database:"+obj.getEmpName());
		 */
		
		  Employee employee=new Employee(37,"evening"); 
		  int ret=dao.updateEmployee(employee);
		 System.out.println("how many records updated:"+ret);
		 
	/*	Employee employee=dao.deleteEmployee(3);
		System.out.println("who was deleted:"+employee.getEmpName());*/

	}

}
