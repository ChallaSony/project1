package com.main;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import  org.hibernate.Transaction;

import com.model.Employee;

public class Demo {

	public static void main(String[] args) {
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("com/config/hibernate.cfg.xml")
                .build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
        SessionFactory factory = meta.getSessionFactoryBuilder().build();
        Session session = factory.openSession();
       // Transaction transaction = session.beginTransaction(); // commit & rollback
       Transaction transaction=session.beginTransaction();
        Employee employee = new Employee(33,"with annotation");
        employee.setEmpName("hibernate demo1"); 
        session.save(employee);
        transaction.commit();  // persist
        System.out.println("successfully saved");
       
        session.close();
        factory.close();
    }
}

