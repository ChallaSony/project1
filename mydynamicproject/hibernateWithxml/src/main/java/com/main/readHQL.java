package com.main;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.NativeQuery;
import org.hibernate.Transaction;

import com.model.Employee;

public class readHQL {

	public static void main(String[] args) {
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("com/config/hibernate.cfg.xml")
				.build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();
		try {
			/*
			 * String hql="from Employee emp where emp.empName='evening' "; Query
			 * query=session.createQuery(hql); // query.setParameter("nq",33);
			 * List<Employee> listOfEmp=query.getResultList();
			 */
			
			/*
			 * Criteria cr = session.createCriteria(Employee.class); //criteria query
			 * cr.add(Restrictions.eq("empName","evening"));
			 *  List<Employee> res = cr.list();
			 */
			
			/*
			 * String nativeQuery="select *from employee_table"; NativeQuery<Employee> nq=
			 * session.createNativeQuery(nativeQuery).addEntity(Employee.class);
			 * List<Employee> res=nq.getResultList();
			 */
			
			//named query
			List<Employee> res=session.getNamedQuery("hi").getResultList();
			for (Employee employee : res) {
				System.out.println("emp no:" + employee.getEmpNo());
				System.out.println("emp name:" + employee.getEmpName());
			}
		} catch (Exception e) {
			System.out.println("end the progam");
		}

	}
}
