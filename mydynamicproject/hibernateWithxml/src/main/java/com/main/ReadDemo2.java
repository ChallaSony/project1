package com.main;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.Transaction;

import com.model.Employee;

public class ReadDemo2 {

	public static void main(String[] args) {
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("com/config/hibernate.cfg.xml")
				.build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();
		// Employee employee=session.get(Employee.class,10);
		Employee employee = session.load(Employee.class, 1);
		if (employee != null) {
			employee.setEmpName("Lunch break"); // update
			Transaction ts = session.beginTransaction();
			session.update(employee);
			ts.commit();
			System.out.println("emp no:" + employee.getEmpNo());
			System.out.println("employee name:" + employee.getEmpName());
			System.out.println("successfully saved");
		} else {
			System.out.println("No recard found");
		}
		session.close();
		factory.close();
	}
}
