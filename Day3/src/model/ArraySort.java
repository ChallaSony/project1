package model;

public class ArraySort {
	public void arraySort(int arr[],int n) {
		int key;
		int j;
		for(int i=1;i<n;i++)
		{
			key=arr[i];
			j=i-1;
			while(j>=0 && arr[j]>key)
			{
				arr[j+1]=arr[j];
				j--;
			}
			arr[j+1]=key;
		}
		System.out.println("Array after sorting:");
		for(int i=0;i<n;i++)
		{
			System.out.println(arr[i]);
		}
		
	}

}
