package model;

public class StringPalindrome {
	public String stringPalindrome(String str)
	{
		int count=0;
		int length=str.length()-1;
		int middle=str.length()/2;
		for(int i=0;i<middle;i++)
		{
			if(str.charAt(i)==str.charAt(length))
			{
				count++;
				length--;
			}
		}
		if(count==middle)
		{
			return "YES";
		}
		else
		{
			return "NO";
		}
	}

}
