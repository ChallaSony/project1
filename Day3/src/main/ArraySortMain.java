package main;
import java.util.Scanner;
import model.ArraySort;
public class ArraySortMain {
	public static void main(String[] args)
	{
		Scanner scanner=new Scanner(System.in);
		System.out.println("enter the array size:");
		int n=scanner.nextInt();
		int arr[]=new int[n];
		System.out.println(" array elements:");
		for(int i=0;i<n;i++)
		{
			arr[i]=scanner.nextInt();
		}
		ArraySort arraysort=new ArraySort();
		arraysort.arraySort(arr, n);
		
	}

}
