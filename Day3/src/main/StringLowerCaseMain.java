package main;
import model.StringLowerCase;
import java.util.Scanner;
public class StringLowerCaseMain {
	public static void main(String[] args)
	{
		Scanner scanner=new Scanner(System.in);
		System.out.println("Enter the string:");
		String str=scanner.next();
		System.out.println("Lowercase of given string is:"+StringLowerCase.stringLower(str));
		
		
	}

}
