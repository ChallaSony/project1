package main;
import java.util.Scanner;
import model.SubString;
public class SubStringMain {
	public static void main(String[] args)
	{
		Scanner scanner=new Scanner(System.in);
		System.out.println("enter the string:");
		String str=scanner.next();
		System.out.println("enter starting index:");
		int first=scanner.nextInt();
		System.out.println("enter ending index:");
		int last=scanner.nextInt();
		System.out.println("Substring for given range is:"+SubString.subString(str, first, last));
		
	}

}
