package main;
import java.util.Scanner;
import model.StringPalindrome;
public class StringPalindromeMain {
	public static void main(String[] args)
	{
		Scanner scanner=new Scanner(System.in);
		System.out.println("enter the string:");
		String str=scanner.next();
		 StringPalindrome palindrome=new  StringPalindrome();
		 String str1=palindrome.stringPalindrome(str);
		 if(str1.equals("YES"))
		 {
			 System.out.println("Given string is palindrome");
		 }
		 else
		 {
			 System.out.println("Given string is not palindrome");
		 }
			 
	 }
}
