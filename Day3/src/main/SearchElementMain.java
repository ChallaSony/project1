package main;

import java.util.Scanner;

import model.SearchElement;

public class SearchElementMain {
	public static void main(String[] args)
	{
		Scanner scanner=new Scanner(System.in);
		System.out.println("enter the array size:");
		int n=scanner.nextInt();
		int arr[]=new int[n];
		System.out.println(" array elements:");
		for(int i=0;i<n;i++)
		{
			arr[i]=scanner.nextInt();
		}
		System.out.println("Enter the element what we have to search:");
		int search=scanner.nextInt();
		SearchElement searchEle=new SearchElement();
		int temp=searchEle.searchElement(arr, n, search);
		if(temp==1)
		{
			System.out.println("element present in the given array");
		}
		else
		{
			System.out.println("element not present in the given array");
		}
	}

}
