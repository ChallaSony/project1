package main;
import java.util.Scanner;

import model.SumOfOddDigits;
public class SumOfOddDigitsMain {
	public static void main(String[] args)
	{
		Scanner scanner=new Scanner(System.in);
		System.out.println("Enter a value:");
		int n=scanner.nextInt();
		int temp=SumOfOddDigits.sumofOddDigits(n);
		if(temp==1)
		{
			System.out.println("Sum of odd digits is odd");
		}
		else
		{
			System.out.println("Sum of odd digits is even");
		}
	}

}
