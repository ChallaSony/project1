package main;
import model.SwapingOfTwoNumbers;
import java.util.Scanner;
public class SwapingOfTwoNumbersMain {
	public static void main(String[] args)
	{
		Scanner scanner=new Scanner(System.in);
		System.out.println("Enter a value:");
		int a=scanner.nextInt();
		System.out.println("Enter b value:");
		int b=scanner.nextInt();
		SwapingOfTwoNumbers swaping=new SwapingOfTwoNumbers();
		swaping.swap(a,b);
		
	}

}
