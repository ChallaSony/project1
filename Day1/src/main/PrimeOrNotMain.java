package main;

import model.PrimeOrNot;
import java.util.Scanner;

public class PrimeOrNotMain {
	public static void main(String[] args)
	{
		Scanner scanner=new Scanner(System.in);
		System.out.println("Enter a value:");
		int a=scanner.nextInt();
		PrimeOrNot prime=new PrimeOrNot();
		int temp=prime.primeorNot(a);
		if(temp==1)
		{
			System.out.println(a+" is a prime number");
		}
		else
		{
			System.out.println(a+" is a not prime number");
		}
		
	}
	

}
