package model;

public class SumOfSquaresOfEvenDigits {
	public static int sumofSquaresofEvenDigits(int n)
	{
		int sum=0;
		int temp;
		while(n!=0)
		{
			temp=n%10;
			n=n/10;
			if(temp%2==0)
			{
				sum+=temp*temp;
			}
		}
		return sum;
	}

}
