package model;

public class LargestWord {
	public static String largestWord(String str)
	{
		String str1[];
		String largest;
		String word;
		str1=str.split(" ");
		largest=str1[0];
		for(int i=1;i<str1.length;i++)
		{
			word=str1[i];
			if(word.length()>largest.length())
			{
				largest=word;
			}
		}
		return largest;
	}

}
