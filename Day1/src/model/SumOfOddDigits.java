package model;

public class SumOfOddDigits {
	public static int sumofOddDigits(int n)
	{
		int sum=0;
		int temp;
		while(n!=0)
		{
			temp=n%10;
			n=n/10;
			if(temp%2!=0)
			{
				sum+=temp;
			}
		}
		if(sum%2==0)
		{
			return -1;
		}
		else
		{
			return 1;
		}
	}

	
}
