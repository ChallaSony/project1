package model;

public class Sixth {
	
	public static int getNumberOfDays(int year,int month)
	{
		int days;
		switch(month)
		{
		case 0:
			days=31;
			return days;	
		case 1:
			if(year%400==0 || year%4==0 && year%100!=0)
			{	days=29;
				return days;
			}
			else
			{
				days=28;
				return days;
			}
		case 2:
			days=31;
			return days;
		case 3:
			days=30;
			return days;
			
		case 4:
			days=31;
			return days;
		case 5:
			days=30;
			return days;	
		case 6:
			days=31;
			return days;
		case 7:
			days=31;
			return days;	
		case 8:
			days=30;
			return days;
		case 9:
			days=31;
			return days;
		case 10:
			days=30;
			return days;
		case 11:
			days=31;
			return days;
		}
		return 0;
	}
}
