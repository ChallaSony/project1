package main;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import model.Thirteen;

public class ThirteenMain {
	public static void main(String[] args) {
	try {
		String date1="03/20/1999";
		String date2="03/15/1999";
		SimpleDateFormat sdf= new SimpleDateFormat("MM/dd/yyyy");
		Date d1=sdf.parse(date1);
		Date d2=sdf.parse(date2);
		int days=Thirteen.getDateDifference(d1,d2);		
		System.out.println(days);
	}
	catch(ParseException E)
	{
		E.printStackTrace();
	}

}}
