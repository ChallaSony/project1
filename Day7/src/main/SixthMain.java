package main;
import java.util.Scanner;

import model.Sixth;
public class SixthMain {

	public static void main(String[] args) {
		Scanner scanner=new Scanner(System.in);
		System.out.println("enter the year:");
		int year=scanner.nextInt();
		System.out.println("enter the month:");
		int month=scanner.nextInt();
		System.out.println("days in given month:"+Sixth.getNumberOfDays(year, month));
	}

}
