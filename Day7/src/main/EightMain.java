package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import model.Eight;
public class EightMain {

	public static void main(String[] args) {
		Scanner scanner=new Scanner(System.in);
		ArrayList<Integer> array1=new ArrayList<>();
		ArrayList<Integer> array2=new ArrayList<>();
		//System.out.println(array1.size());
		System.out.println("enter array1 elements");
		for(int i=0;i<5;i++)
		{
			array1.add(scanner.nextInt());
		}
		System.out.println("enter array2 elements");
		for(int i=0;i<5;i++)
		{
			array2.add(scanner.nextInt());
		}
		
		Eight.sortMergedArrayList(array1,array2);

	}

}
